<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">TestHub</h1>
<h4 align="center" style="margin: 30px 0 30px; font-weight: bold;">流程编排，插件驱动，测试无限可能</h4>
<p align="center">
<a href="https://gitee.com/dromara/TestHub/stargazers"><img src="https://gitee.com/dromara/TestHub/badge/star.svg?theme=gvp"></a>
<a href="https://gitee.com/dromara/TestHub/blob/master/LICENSE"><img src="https://img.shields.io/badge/license-Apache--2.0-green"></a>
<a href="https://gitee.com/dromara/TestHub"><img src="https://img.shields.io/badge/version-v1.0.0-blue"></a>
</p>

## 介绍

   TestHub是一个创新的开源自动化测试工具，它通过引入流程编排功能，采用结构化和模块化的方法，TestHub能够将测试用例表示为一系列可执行的流程，从而实现测试的自动化和标准化。彻底改变了测试过程的管理与执行方式。该工具以其插件式架构和直观的图形化界面，支持用户轻松定义、管理和执行复杂的测试流程。TestHub不仅涵盖了执行测试用例、数据准备和环境配置等步骤，还允许通过简单的拖放和连接操作来构建完整的测试流程。

   TestHub的工作原理基于将复杂的测试过程分解为可复用和可编排的组件，从而实现测试流程的自动化。其核心是一个灵活的流程编排引擎，它允许用户通过连接不同的测试步骤（actions），如数据库操作、HTTP请求、数据校验等，来构建测试流程（flows）。每个步骤可以包含具体的参数（params），支持环境级、用例级和行为级的参数声明，以适应不同测试场景的需求。

   TestHub支持多种数据类型，包括基础类型（如数值、字符串、布尔值）和复合类型（如列表、元数据），以及通过Meta组件自定义的复杂数据结构。此外，它提供了丰富的操作数（formula），包括固定值、变量值、方法调用、算数运算和混合表达式，以及关系运算符和逻辑运算符，用于构建复杂的表达式和逻辑。
核心特性：
    1、灵活的流程编排：TestHub允许用户通过拖放的方式直观地编排测试流程，支持循环、条件分支等控制结构，使得复杂的测试场景变得易于管理和执行。
    2、强大的脚本执行能力：内置了丰富的Action库，包括数据库操作、HTTP请求、数据校验等，用户可以通过编写脚本将这些操作编排进测试流程中。
    3、插件化扩展机制：采用模块化的设计理念，支持通过插件扩展测试功能，使得TestHub能够适应不断变化的测试需求和技术进步。
    4、跨平台支持：作为开源工具，TestHub支持在多种操作系统上运行，包括Windows、MacOS、Web确保了广泛的可用性。

选择 TestHub 自动化测试工具，流程编排驱动测试流程。优化测试流程，释放更多时间和资源创新品质。加入 TestHub，引领自动化测试未来！

[gitee](https://gitee.com/dromara/TestHub) [github](https://github.com/dromara/TestHub)

## 工程目录

<div align="center">

| 目录   | 说明                           |
| ------ | ------------------------------ |
| nsrule | NsRule 规则引擎源码            |
| server | TestHub 后端源码               |
| client | TestHub 前端源码               |
| doc    | TestHub 使用手册源码           |
| docker | TestHub 的 docker 镜像构建脚本 |
| static | markdown 文件资源              |
| demo   | TestHub 的演示 Demo            |

</div>

<div align="center">
    <img src="https://gitee.com/dromara/TestHub/raw/main/TestHub-static/img/概览.jpg" alt="Image" width="100%" >
</div>

## 演示视频

https://www.bilibili.com/video/BV1X94y1v7ak/?spm_id_from=333.337.search-card.all.click&vd_source=adbd50ab0dcce0aafbb00e7a8acb9211

## 演示地址

演示项目地址： http://testhub.nsrule.com:11018
账户：admin 密码 123456

<span style="color:red;"><b>需要测试的话将原有的测试用例 xml 复制后，新增一个你自己的</b></span>

演示环境有问题 请微信：

<div align="center">
    <img src="https://gitee.com/dromara/TestHub/raw/main/TestHub-static/contact.jpeg" alt="Image" width="300" >
</div>

## 部署

### 安装包下载

Testhub 1.0.4: https://url37.ctfile.com/d/42659137-60695737-5e2d86?p=3710 (访问密码: 3710)

### docker 部署 演示 demo

获取项目源码

```
git clone https://gitee.com/dromara/TestHub.git
```

切换到 演示 demo 的 docker compose 配置中

```
cd TestHub/TestHub-demo/TestHubDemo/
```

启动 TestHubDemo

```
docker-compose up -d
```

执行成功后会启动以下服务

<div align="center">

| 服务名   | 服务说明          | 宿主机端口             | 容器 IP     | 容器端口               | 备注                    |
| -------- | ----------------- | ---------------------- | ----------- | ---------------------- | ----------------------- |
| testhub  | TestHub 项目      | 11018 前端；12003 后端 | 192.168.0.3 | 11018 前端；12003 后端 |                         |
| server   | 被测试的 web 项目 | 12004                  | 192.168.0.4 | 12004 http 接口        |                         |
| postgres | 被测试的数据库    | 12005                  | 192.168.0.5 | 5432                   | 用户名密码均为 postgres |

</div>

前端接口地址 http://127.0.0.1:11018/

后端接口文档 http://127.0.0.1:12003/swagger-ui/index.html#/

### docker 部署 TestHub

获取 TestHub 镜像

```
docker pull vinc02131/testhub:1.0.4
```

运行

```
docker run -d -p 12003:12003 -p 11018:11018 vinc02131/testhub:1.0.4
```

前端接口地址 http://127.0.0.1:11018/

后端接口文档 http://127.0.0.1:12003/swagger-ui/index.html#/

### 源码部署 TestHub

获取项目源码

```
git clone https://gitee.com/dromara/TestHub.git
```

**启动后台服务**

依赖 jdk17

1. 安装 nsrule 依赖。
   nsrule 项目 暂未发布中央仓库，需要打开 nsrule 的工程执行 mvn install 安装到本地仓库
2. 打开 server 中的 TestHub 项目，执行 TestHubApplication
3. 后端接口文档 : http://127.0.0.1:12003

**启动前端服务**

依赖 node v19.5.0

1. 命令行切换到 front 目录中。
2. 导入项目依赖

```
   yarn install
```

3. 启动项目

```
   yarn start
```

4. 前端地址 : http://localhost:8000

## 迭代方向

<div align="center">

| 功能               | 支持 |
| ------------------ | ---- |
| 执行计划管理       |      |
| 百宝箱             |      |
| websocket 测试工具 |      |
| mock 模块          |      |
| 前端拖拉拽配置     |      |

</div>

## 软件界面

<div align="center">
    <img src="https://gitee.com/dromara/TestHub/raw/main/TestHub-static/img/环境级参数.jpg" alt="Image" width="100%" >
</div>

<div align="center">
    <img src="https://gitee.com/dromara/TestHub/raw/main/TestHub-static/img/全局行为.jpg" alt="Image" width="100%" >
</div>

<div align="center">
    <img src="https://gitee.com/dromara/TestHub/raw/main/TestHub-static/img/check_loop.png" alt="Image" width="100%" >
</div>

<div align="center">
    <img src="https://gitee.com/dromara/TestHub/raw/main/TestHub-static/img/check_obj.png" alt="Image" width="100%" >
</div>

<div align="center">
    <img src="https://gitee.com/dromara/TestHub/raw/main/TestHub-static/img/check.png" alt="Image" width="100%" >
</div>

<div align="center">
    <img src="https://gitee.com/dromara/TestHub/raw/main/TestHub-static/img/convert.png" alt="Image" width="100%" >
</div>

<div align="center">
    <img src="https://gitee.com/dromara/TestHub/raw/main/TestHub-static/img/http.png" alt="Image" width="100%" >
</div>

<div align="center">
    <img src="https://gitee.com/dromara/TestHub/raw/main/TestHub-static/img/sql.png" alt="Image" width="100%" >
</div>
